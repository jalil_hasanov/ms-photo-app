package com.photoapp.users.mapstruct;

import com.photoapp.users.ui.entity.UsersEntity;
import com.photoapp.users.ui.model.AlbumResponseModel;
import com.photoapp.users.ui.model.CreateUserDetailsDto;
import com.photoapp.users.ui.model.UserResponseModel;
import com.photoapp.users.ui.response.CreateUserResponse;
import com.photoapp.users.ui.shared.UserDto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-13T19:36:02+0400",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.1.jar, environment: Java 19.0.2 (Oracle Corporation)"
)
@Component
public class UsersMapStructImpl implements UsersMapStruct {

    @Override
    public UsersEntity mapUserDtoToUsersEntity(UserDto dto) {
        if ( dto == null ) {
            return null;
        }

        UsersEntity usersEntity = new UsersEntity();

        usersEntity.setFirstName( dto.firstName() );
        usersEntity.setLastName( dto.lastName() );
        usersEntity.setEmail( dto.email() );
        usersEntity.setUserId( dto.userId() );
        usersEntity.setEncryptedPassword( dto.encryptedPassword() );

        return usersEntity;
    }

    @Override
    public CreateUserResponse mapUsersEntityToUserResponse(UsersEntity save) {
        if ( save == null ) {
            return null;
        }

        String firstName = null;
        String lastName = null;
        String email = null;
        String userId = null;

        firstName = save.getFirstName();
        lastName = save.getLastName();
        email = save.getEmail();
        userId = save.getUserId();

        CreateUserResponse createUserResponse = new CreateUserResponse( firstName, lastName, email, userId );

        return createUserResponse;
    }

    @Override
    public UserDto mapUserEntityToUserDto(UsersEntity byEmail) {
        if ( byEmail == null ) {
            return null;
        }

        String firstName = null;
        String lastName = null;
        String email = null;
        String userId = null;
        String encryptedPassword = null;

        firstName = byEmail.getFirstName();
        lastName = byEmail.getLastName();
        email = byEmail.getEmail();
        userId = byEmail.getUserId();
        encryptedPassword = byEmail.getEncryptedPassword();

        String password = null;

        UserDto userDto = new UserDto( firstName, lastName, password, email, userId, encryptedPassword );

        return userDto;
    }

    @Override
    public UserDto mapCreateUserDetailsDtoToUserDto(CreateUserDetailsDto createUserDetailsDto) {
        if ( createUserDetailsDto == null ) {
            return null;
        }

        String firstName = null;
        String lastName = null;
        String password = null;
        String email = null;

        firstName = createUserDetailsDto.firstName();
        lastName = createUserDetailsDto.lastName();
        password = createUserDetailsDto.password();
        email = createUserDetailsDto.email();

        String userId = null;
        String encryptedPassword = null;

        UserDto userDto = new UserDto( firstName, lastName, password, email, userId, encryptedPassword );

        return userDto;
    }

    @Override
    public UserResponseModel mapToUserResponseModel(UsersEntity user, List<AlbumResponseModel> albums) {
        if ( user == null && albums == null ) {
            return null;
        }

        String userId = null;
        String firstName = null;
        String lastName = null;
        String email = null;
        if ( user != null ) {
            userId = user.getUserId();
            firstName = user.getFirstName();
            lastName = user.getLastName();
            email = user.getEmail();
        }
        List<AlbumResponseModel> albums1 = null;
        List<AlbumResponseModel> list = albums;
        if ( list != null ) {
            albums1 = new ArrayList<AlbumResponseModel>( list );
        }

        UserResponseModel userResponseModel = new UserResponseModel( userId, firstName, lastName, email, albums1 );

        return userResponseModel;
    }
}
