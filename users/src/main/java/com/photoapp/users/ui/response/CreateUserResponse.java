package com.photoapp.users.ui.response;

public record CreateUserResponse(
        String firstName,
        String lastName,
        String email,
        String userId
) {
}
