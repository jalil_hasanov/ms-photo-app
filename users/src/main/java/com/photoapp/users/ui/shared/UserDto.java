package com.photoapp.users.ui.shared;


import com.ctc.wstx.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.Serializable;
import java.util.UUID;

public record UserDto (
        String firstName,
        String lastName,
        String password,
        String email,
        String userId,
        String encryptedPassword
) implements Serializable{
        @Override
        public String userId() {
                return UUID.randomUUID().toString();
        }

        @Override
        public String encryptedPassword() {
                if (StringUtils.isEmpty(encryptedPassword)){
                        return new BCryptPasswordEncoder().encode(password);
                }else {
                        return encryptedPassword;
                }
        }
}
