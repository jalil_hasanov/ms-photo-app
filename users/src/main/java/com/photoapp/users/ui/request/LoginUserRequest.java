package com.photoapp.users.ui.request;

public record LoginUserRequest(
        String email,
        String password
) {
}
