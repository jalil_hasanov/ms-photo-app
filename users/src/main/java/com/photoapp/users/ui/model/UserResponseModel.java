package com.photoapp.users.ui.model;

import java.util.List;

public record UserResponseModel(
        String userId,
        String firstName,
        String lastName,
        String email,
        List<AlbumResponseModel> albums
) {

}
