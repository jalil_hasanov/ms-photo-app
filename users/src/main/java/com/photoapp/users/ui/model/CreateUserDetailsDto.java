package com.photoapp.users.ui.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public record CreateUserDetailsDto(
        @NotEmpty(message = "First name cannot be neither null nor empty.")
        @Size(min = 2, max = 20, message = "First name length does not match the required boundaries.")
        String firstName,
        @NotEmpty(message = "Last name cannot be neither null nor empty.")
        @Size(min = 2, max = 20, message = "Last name length does not match the required boundaries.")
        String lastName,
        @NotEmpty(message = "Password cannot be neither null nor empty.")
        @Size(min = 8, max = 16, message = "Password length does not match the required boundaries.")
        String password,
        @Email
        String email
) {
}
