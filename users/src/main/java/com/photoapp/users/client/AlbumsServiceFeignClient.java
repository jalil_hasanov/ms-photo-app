package com.photoapp.users.client;

import com.photoapp.users.ui.model.AlbumResponseModel;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@FeignClient(name = "albums-ms", url = "${feign.client.config.account-ms-url.url}")
public interface AlbumsServiceFeignClient {

    @GetMapping(value = "/{id}")
    @Retry(name = "albums-ms-retry")
    @CircuitBreaker(name = "albums-ms-cb", fallbackMethod = "userAlbumsFallback")
    ResponseEntity<List<AlbumResponseModel>> userAlbums(@PathVariable("id") String id);

    default ResponseEntity<List<AlbumResponseModel>> userAlbumsFallback(String id, Throwable exception) {
        System.out.println("method param: "+id);
        System.out.println("exception message: "+exception.getMessage());
        return ResponseEntity.ok(new ArrayList<>());
    }
}



