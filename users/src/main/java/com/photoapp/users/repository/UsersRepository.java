package com.photoapp.users.repository;

import com.photoapp.users.ui.entity.UsersEntity;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<UsersEntity,Long> {
    UsersEntity findByEmail(String email);
    UsersEntity findByUserId(String userId);
}
