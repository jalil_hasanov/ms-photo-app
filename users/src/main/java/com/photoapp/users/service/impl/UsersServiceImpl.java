package com.photoapp.users.service.impl;

import com.photoapp.users.client.AlbumsServiceFeignClient;
import com.photoapp.users.mapstruct.UsersMapStruct;
import com.photoapp.users.repository.UsersRepository;
import com.photoapp.users.service.UsersService;
import com.photoapp.users.ui.entity.UsersEntity;
import com.photoapp.users.ui.model.AlbumResponseModel;
import com.photoapp.users.ui.model.CreateUserDetailsDto;
import com.photoapp.users.ui.model.UserResponseModel;
import com.photoapp.users.ui.response.CreateUserResponse;
import com.photoapp.users.ui.shared.UserDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UsersServiceImpl implements UsersService {
    UsersMapStruct usersMapStruct;
    UsersRepository usersRepository;
    AlbumsServiceFeignClient albumsServiceFeignClient;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public CreateUserResponse createUser(CreateUserDetailsDto createUserDetailsDto) {
        UserDto dto = usersMapStruct.mapCreateUserDetailsDtoToUserDto(createUserDetailsDto);
        UsersEntity usersEntity = usersMapStruct.mapUserDtoToUsersEntity(dto);
        return usersMapStruct.mapUsersEntityToUserResponse(usersRepository.save(usersEntity));
    }

    @Override
    public UserDto getUserByEmail(String email) {
        UsersEntity usersEntity = Optional.of(usersRepository.findByEmail(email)).orElseThrow(
                () -> new UsernameNotFoundException("User with email '"+email+"' not found!")
        );
        return usersMapStruct.mapUserEntityToUserDto(usersEntity);
    }

    @Override
    public UserResponseModel getUser(String userId) {
        UsersEntity user = usersRepository.findByUserId(userId);
        logger.debug("Before calling albums-ms");
        List<AlbumResponseModel> albums = albumsServiceFeignClient.userAlbums(userId).getBody();
        logger.debug("After calling albums-ms");
        return usersMapStruct.mapToUserResponseModel(user, albums);
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<UsersEntity> usersEntityOptional = Optional.of(usersRepository.findByEmail(email));
        UsersEntity usersEntity =
                usersEntityOptional.orElseThrow(() -> new RuntimeException("user with email: "+email+" not found."));
        return new User(
                usersEntity.getEmail(),
                usersEntity.getEncryptedPassword(),
                true,
                true,
                true,
                true,new ArrayList<>()
        );
    }


}
