package com.photoapp.users.service;

import com.photoapp.users.ui.model.CreateUserDetailsDto;
import com.photoapp.users.ui.model.UserResponseModel;
import com.photoapp.users.ui.response.CreateUserResponse;
import com.photoapp.users.ui.shared.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UsersService extends UserDetailsService {
    CreateUserResponse createUser(CreateUserDetailsDto dto);

    UserDto getUserByEmail(String email);

    UserResponseModel getUser(String userId);
}
