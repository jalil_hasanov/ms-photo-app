package com.photoapp.users.mapstruct;

import com.photoapp.users.ui.entity.UsersEntity;
import com.photoapp.users.ui.model.AlbumResponseModel;
import com.photoapp.users.ui.model.CreateUserDetailsDto;
import com.photoapp.users.ui.model.UserResponseModel;
import com.photoapp.users.ui.response.CreateUserResponse;
import com.photoapp.users.ui.shared.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UsersMapStruct {

    @Mapping(target = "id", ignore = true)
    UsersEntity mapUserDtoToUsersEntity(UserDto dto);

    CreateUserResponse mapUsersEntityToUserResponse(UsersEntity save);
    @Mapping(target = "password", ignore = true)
    UserDto mapUserEntityToUserDto(UsersEntity byEmail);

    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "encryptedPassword", ignore = true)
    UserDto mapCreateUserDetailsDtoToUserDto(CreateUserDetailsDto createUserDetailsDto);

    @Mapping(target = "albums", source = "albums")
    UserResponseModel mapToUserResponseModel(UsersEntity user, List<AlbumResponseModel> albums);
}
