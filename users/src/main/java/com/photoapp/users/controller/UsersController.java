package com.photoapp.users.controller;

import com.photoapp.users.service.UsersService;
import com.photoapp.users.ui.model.CreateUserDetailsDto;
import com.photoapp.users.ui.model.UserResponseModel;
import com.photoapp.users.ui.response.CreateUserResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Slf4j
public class UsersController {

    private final Environment environment;
    private final UsersService service;

    @GetMapping("/health/check")
    public String health(){
        log.info("users-ms working on port: "+environment.getProperty("local.server.port")+
                ", token = "+environment.getProperty("token.secret"));
        return "users-ms working on port: "+environment.getProperty("local.server.port")+
                ", token = "+environment.getProperty("token.secret");
    }

    @PostMapping("/create")
    public ResponseEntity<CreateUserResponse> create(@Valid @RequestBody CreateUserDetailsDto request){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.createUser(request));
    }

    @GetMapping(value = "/{userId}",
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            })
    public ResponseEntity<UserResponseModel> getUser(@PathVariable("userId") String userId){
        return ResponseEntity.ok(service.getUser(userId));
    }

}

