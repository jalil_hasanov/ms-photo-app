package com.photoapp.users.config.feign;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignLogger {

    @Bean
    Logger.Level feignClientLogger(){
        return Logger.Level.FULL;
    }


}
