package com.photoapp.users.config.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.photoapp.users.service.UsersService;
import com.photoapp.users.ui.request.LoginUserRequest;
import com.photoapp.users.ui.shared.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;

import static java.time.Instant.now;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    UsersService usersService;
    Environment environment;
    public static Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    public AuthenticationFilter(
            UsersService usersService,
            Environment environment,
            AuthenticationManager authenticationManager
    ) {
                super(authenticationManager);
                this.usersService= usersService;
                this.environment = environment;
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest req,
            HttpServletResponse res
    ) throws AuthenticationException {
        try {
            LoginUserRequest loginUserRequest = new ObjectMapper()
                    .readValue(req.getInputStream(), LoginUserRequest.class);
            logger.info(loginUserRequest.email());
            return getAuthenticationManager().authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginUserRequest.email(),
                            loginUserRequest.password(),
                            new ArrayList<>()
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication auth
    ) throws IOException, ServletException {
        String userName = ((User)auth.getPrincipal()).getUsername();
        UserDto userDetails = usersService.getUserByEmail(userName);
        Instant now = now();
        String tokenKey = environment.getProperty("token.secret");
        logger.info(tokenKey);
        byte[] secretKeyBytes = Base64.getEncoder().encode(tokenKey.getBytes());
        SecretKey secretKey = new SecretKeySpec(
                secretKeyBytes,
                SignatureAlgorithm.HS256.getJcaName()
        );

        String token = Jwts.builder()
                            .setSubject(userDetails.userId())
                            .setExpiration(Date.from(now
                                    .plusMillis(Long.parseLong(environment.getProperty("token.expiration_time"))))
                            )
                            .setIssuedAt(Date.from(now))
                            .signWith(secretKey, SignatureAlgorithm.HS256)
                            .compact();
        logger.info(environment.getProperty("token.expiration_time"));
        response.addHeader("token", token);
        response.addHeader("userId", userDetails.userId());
    }

}
