package com.photoapp.users.config.security;

import com.photoapp.users.config.security.filters.AuthenticationFilter;
import com.photoapp.users.service.UsersService;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;



@Configuration
@EnableWebSecurity
public class WebSecurity{

    private final Environment environment;
    private final UsersService usersService;

    public WebSecurity(Environment environment, UsersService usersService) {
        this.environment = environment;
        this.usersService = usersService;
    }

    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        /* Configure AuthenticationManagerBuilder */
        AuthenticationManagerBuilder authenticationManagerBuilder = http
                .getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder
                .userDetailsService(usersService)
                .passwordEncoder(passwordEncoder());
        AuthenticationManager authenticationManager = authenticationManagerBuilder.build();
        http
                .csrf()
                .disable();
        AuthenticationFilter authenticationFilter =
                new AuthenticationFilter(usersService,environment,authenticationManager);

        authenticationFilter.setFilterProcessesUrl(environment.getProperty("login.url.path"));

        http
                .authorizeHttpRequests()
                .requestMatchers("/users/**")
                .permitAll()
                .requestMatchers(new AntPathRequestMatcher("/h2-console/**")).permitAll()
                .requestMatchers(HttpMethod.GET,"/actuator/**").permitAll()
                .and()
                .addFilter(authenticationFilter)
                .authenticationManager(authenticationManager)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .headers()
                .frameOptions()
                .disable();
        return http.build();
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
