package com.photoapp.users;

import com.photoapp.users.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = UsersService.class)
class UsersApplicationTests {

	@Test
	void contextLoads() {

	}

}
