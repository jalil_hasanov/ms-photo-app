package com.photoapp.albums.service;

import com.photoapp.albums.model.response.AlbumResponseModel;

import java.util.List;

public interface AlbumsService {
    List<AlbumResponseModel> userAlbums(String id);

}
