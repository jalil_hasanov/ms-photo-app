package com.photoapp.albums.service.impl;

import com.photoapp.albums.model.response.AlbumResponseModel;

import com.photoapp.albums.service.AlbumsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlbumsServiceImpl implements AlbumsService {

    @Override
    public List<AlbumResponseModel> userAlbums(String id) {
        AlbumResponseModel album1 = new AlbumResponseModel(
                "album1Id",
                "3142342342342134",
                "album 1 name",
                "album 1 description");
        AlbumResponseModel album2 = new AlbumResponseModel(
                "album2Id",
                "3242342342342134",
                "album 2 name",
                "album 2 description");
        return new ArrayList<>(List.of(album1, album2));
    }

}
