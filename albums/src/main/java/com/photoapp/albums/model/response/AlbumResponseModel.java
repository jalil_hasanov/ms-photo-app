package com.photoapp.albums.model.response;

public record AlbumResponseModel(
        String albumId,
        String userId,
        String name,
        String description
) {
}
