package com.photoapp.albums.controller;

import com.photoapp.albums.model.response.AlbumResponseModel;
import com.photoapp.albums.service.AlbumsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/albums")
public class AlbumsController {

    private final AlbumsService albumsService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<List<AlbumResponseModel>> userAlbums(@PathVariable("id") String id){
        return ResponseEntity.ok(albumsService.userAlbums(id));
    }

}
