package com.photoapp.apigateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

@Configuration
public class GlobalFiltersConfiguration {
    private final Logger logger = LoggerFactory.getLogger(GlobalFiltersConfiguration.class);

    @Order(1)
    @Bean
    public GlobalFilter secondPreFilter(){
        return ((exchange, chain) -> {
            logger.info("My second pre-filter is executed...");
            return chain.filter(exchange);
        });
    }

    @Order(2)
    @Bean
    public GlobalFilter thirdPreFilter(){
        return ((exchange, chain) -> {
            logger.info("My third pre-filter is executed...");
            return chain.filter(exchange);
        });
    }

    @Order(3)
    @Bean
    public GlobalFilter forthPreFilter(){
        return ((exchange, chain) -> {
            logger.info("My forth pre-filter is executed...");
            return chain.filter(exchange);
        });
    }

    @Order(2)
    @Bean
    public GlobalFilter secondPostFilter(){
        return ((exchange, chain) ->
             chain.filter(exchange).then(
                    Mono.fromRunnable(
                            () -> logger.info("My second post filter is executed...")
                    )
            )
        );
    }

    @Order(1)
    @Bean
    public GlobalFilter thirdPostFilter(){
        return ((exchange, chain) ->
                chain.filter(exchange).then(
                        Mono.fromRunnable(
                                () -> logger.info("My third post filter is executed...")
                        )
                )
        );
    }

    @Order(0)
    @Bean
    public GlobalFilter forthPostFilter(){
        return ((exchange, chain) ->
                chain.filter(exchange).then(
                        Mono.fromRunnable(
                                () -> logger.info("My forth post filter is executed...")
                        )
                )
        );
    }
}
