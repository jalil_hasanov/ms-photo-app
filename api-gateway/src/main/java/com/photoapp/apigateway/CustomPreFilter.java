package com.photoapp.apigateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Set;

@Order(0)
@Component
public class CustomPreFilter implements GlobalFilter {
    private final Logger logger = LoggerFactory.getLogger(CustomPreFilter.class);

    @Override
    public Mono<Void> filter(
            ServerWebExchange exchange,
            GatewayFilterChain chain ) {
        logger.info("custom pre-filter executed... ");
        String requestPath =  exchange.getRequest().getPath().toString();
        logger.info("Request path = "+requestPath);
        HttpHeaders httpHeaders = exchange.getRequest().getHeaders();
        Set<String> headerNames = httpHeaders.keySet();
        headerNames.forEach(
                (headerName) -> {
                    String headerValue =httpHeaders.getFirst(headerName);
                    logger.info("headerName - "+headerName+": "+"value - "+headerValue);
                }
        );
        return chain.filter(exchange);
    }
}
